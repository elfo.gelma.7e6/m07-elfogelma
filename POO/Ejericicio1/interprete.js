class Interprete{
    #nomInterprete;
    #apellido;
    #nomArtistico;

    constructor(){
        this.#nomInterprete = "";
        this.#apellido = "";
        this.#nomArtistico = "";
    }

    getNomInterprete(){
        return this.#nomInterprete;
    }
    getApellido(){
        return this.#apellido;
    }
    getNomArtistico(){
        return this.#nomArtistico;
    }

    addInfo(nomInterprete, apellido, nomArtistico){
        this.#nomInterprete = nomInterprete;
        this.#apellido = apellido;
        this.#nomArtistico = nomArtistico;
    }

    showInfo(){
        console.log("Interprete: " + this.#nomArtistico)
        console.log("       Nombre: " + this.#nomInterprete + " " + this.#apellido);
    }

    returnString(){
        return this.#nomInterprete + " " + this.#apellido + " " + this.#nomArtistico;
    }
}