src="disco.js";
src="discografia.js";
src="interprete.js";

let discografia = new Discografia();

fetch('discos.json')
   .then(function (response) {
       return response.json();
   })
   .then(function (dataJson) {
       for (const disc of dataJson.discos) {
            let disco = new Disco();
            let autor = new Interprete();
            autor.addInfo(disc.interprete.nomInterprete, disc.interprete.apellido, disc.interprete.nomArtistico);
            disco.addInfo(disc.nombre, autor, disc.year, disc.tipo, disc.imagen, disc.localizacion);
            discografia.pushDisco(disco);
       }

   })
   .catch(function (error) { console.log("Algo ha salido mal: "+ error) })