src="interprete.js";
class Disco{
    nombre;
    interprete;
    year;
    tipo;
    imagen;
    #localizacion;
    #prestado;

    constructor(){
        this.nombre = "";
        this.interprete = "";
        this.year = "";
        this.tipo = "";
        this.imagen = "";
        this.#localizacion = 0;
        this.#prestado = false;
    }    
    addInfo(nombre, interprete, year, tipo, imagen, localizacion){
        this.nombre = nombre; 
        this.interprete = interprete;
        this.year = year;
        this.tipo = tipo;
        this.imagen = imagen;
        this.#localizacion = localizacion;
        this.#prestado = false;
    }

    getLocalizacion(){
        return this.#localizacion;
    }
    getPrestado(){
        return this.#prestado;
    }

    showInfo(){
        console.log("Nombre: " + this.nombre);
        this.interprete.showInfo();
        console.log("Año: " + this.year);
        console.log("Tipo: " + this.tipo);
        console.log("Loacalizacion: " + this.#localizacion);
        console.log("Prestado: " + this.#prestado);
    }
    returnString(){
        return this.nombre + " " + this.year + " " + this.tipo + " " + this.imagen + " " + this.#localizacion + " " + this.#prestado;
    }


}