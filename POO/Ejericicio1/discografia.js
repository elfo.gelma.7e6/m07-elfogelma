src="disco.js";
src="interprete.js";
class Discografia{
    constructor(){
        this.discografia = [];
    }
    //Añade un disco a la discografia
    pushDisco(disco){
        this.discografia.push(disco);
    }

    deleteDisco(nombreDisco){
        var count = 0;
        for(var disco of this.discografia){
            if(disco.nombre == nombreDisco){
                this.discografia.splice(count,1);
            }
            count++;
        }
        
    }

    orderDisco(atributo){
        
        switch(atributo){
            case "nomInterprete":
                this.discografia.sort((a,b) => {
                    if(a.interprete.getNomInterprete() > b.interprete.getNomInterprete()) return 1;
                    else if (a.interprete.getNomInterprete() < b.interprete.getNomInterprete()) return -1;
                    return 0;
                }) 
                break;
            case "apellido":
                this.discografia.sort((a,b) => {
                    if(a.interprete.getApellido() > b.interprete.getApellido()) return 1;
                    else if (a.interprete.getApellido() < b.interprete.getApellido()) return -1;
                    return 0;
                }) 
                break;
            case "nomArtistico":
                this.discografia.sort((a,b) => {
                    if(a.interprete.getNomArtistico() > b.interprete.getNomArtistico()) return 1;
                    else if (a.interprete.getNomArtistico() < b.interprete.getNomArtistico()) return -1;
                    return 0;
                }) 
                break;
            default:
                this.discografia.sort((a,b) => {
                    if(a[atributo] > b[atributo]) return 1;
                    else if (a[atributo] < b[atributo]) return -1;
                    return 0;
                })
                break;

        }                       
    }

    show(){
        console.log("------DISCOGRAFIA------");
        for(var disco of this.discografia){
            console.log("   ");
            disco.showInfo();
        }
    }

    
}