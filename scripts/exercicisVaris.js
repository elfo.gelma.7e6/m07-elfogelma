function ex1(){
    var numero1 = parseInt(prompt("Introduce un valor: "));
    var numero2 = parseInt(prompt("Introduce otro valor: "));
    console.log("El valor mayor es:"+Mayor(numero1, numero2));
}

function ex2(){
    var numero1 = parseInt(prompt("Introduce un valor: "));
    var numero2 = parseInt(prompt("Introduce otro valor: "));
    console.log("El producto es:"+MultiplacionSinAsterisco(numero1, numero2));

}

function ex3(){
    var numero1 = parseInt(prompt("Introduce un valor: "));
    var numero2 = parseInt(prompt("Introduce otro valor: "));
    var numero3 = parseInt(prompt("Introduce un tercer valor: "));
    console.log("El producto es:"+MultiplicacionTriple(numero1, numero2, numero3));

}

function ex4(){
    var suma;
    for (var i = 1; i <= 10000; i++) {
        suma = SumaDelCubo1000Numeros(i);
        if(i == suma){
            console.log(i);
        }
    }
    
}

function ex5(){
    var nota1 = parseInt(prompt("Introduce un valor: "));
    var nota2 = parseInt(prompt("Introduce otro valor: "));
    var nota3 = parseInt(prompt("Introduce un tercer valor: "));
    console.log("La nota media es: " + Media3Notas(nota1,nota2,nota3));

}

function ex6(){
    var numero = prompt("Introduce un valor: ");
    switch(EsEntero(numero)){
        case 0:
            console.log(AlCubo(parseInt(numero)));
            console.log(Math.pow(5,3));
            break;
        case 1:
            alert("El valor introducido no es entero, sinó string");
            break;
        case 2:
            alert("El valor introducido no es entero, sinó decimal");
            break;

    }


}

//Funciones menores

//Retorna el numero major.
function Mayor(n1, n2){
    if(n1 > n2) return n1;
    return n2;
}
//Multiplicacio de dos valors.
function MultiplacionSinAsterisco(n1, n2){
    var total = 0;
    for(i = 0; i < n2;i++){
        total += n1;
    }
    return total;
}
//Multipicacio de tres valors
function MultiplicacionTriple(n1,n2,n3){
    var total = 0;
    total = MultiplacionSinAsterisco(n1, n2);
    total = MultiplacionSinAsterisco(total, n3);
    return total;
}
//Retorna la suma dels digits al cub d'un numero.
function SumaDelCubo1000Numeros(numero){
    var suma = 0;
    while(numero > 0){
        suma = parseInt(suma + AlCubo(Descomposicion(numero)));
        numero = parseInt(numero / 10);
    }
    return suma;

}
//Retorna la potència al cub d'un numero.
function AlCubo(numero){
    numero = MultiplicacionTriple(numero,numero,numero);
    return numero;
}

function Descomposicion(numero){
    return numero%10;
}

function Media3Notas(nota1, nota2, nota3){
    return  ((nota1+nota2+nota3)/3).toFixed(1);
    
}

function EsEntero(numero){
    if(numero % 1 == 0){
        return 0;
    }else if(isNaN(numero)){
        return 1;
    }else return 2;
}

