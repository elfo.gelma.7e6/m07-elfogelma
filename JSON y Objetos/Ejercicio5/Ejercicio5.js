// const requestURL = 'info.json';
// const request = new XMLHttpRequest();
// request.open('GET', requestURL);
// request.responseType = 'json';
// request.send();
// request.onload = function() {
//     const jsonObj = request.response;
//     console.log(jsonObj);
//     let text = "<table border='1'>"
//     jsonObj.stations.sort(function(a,b){ return b.bikes - a.bikes});
//     for (const bici of jsonObj.stations) {
        
//         if(bici.bikes > 3){
//             text += "<tr><td>" + bici.id + "</td><td>" + bici.streetName + "</td><td>" + bici.longitude + "</td><td>" + bici.latitude + "</td><td>" + bici.slots + "</td><td>" + bici.bikes + "</td></tr>";
//         }
//     }
//     text += "</table>"
//     document.getElementById("demo").innerHTML = text;
// }
fetch('info.json')
   .then(function (response) {
       return response.json();
   })
   .then(function (dataJson) {
       console.log(dataJson);
       let text = "<table border='1'>"
       let newText = "</br>"
       let maxSlots = 0;
       dataJson.stations.sort(function(a,b){ return b.bikes - a.bikes});
       for (const bici of dataJson.stations) {
            if(bici.bikes > 3){
                text += "<tr><td>" + bici.id + "</td><td>" + bici.streetName + "</td><td>" + bici.longitude + "</td><td>" + bici.latitude + "</td><td>" + bici.slots + "</td><td>" + bici.bikes + "</td></tr>";
            }
            if(maxSlots < bici.slots) maxSlots = bici.slots
       }
       let num33 = dataJson.stations.find(station => station.id == 33);
       newText += num33.streetName + ", " + num33.streetNumber + "</br>";

       let latitude41 = dataJson.stations.find(station => station.latitude == 41.398389)
       newText += latitude41.streetName + ", " + latitude41.streetNumber + "  --Slots: " + latitude41.slots + "</br>";

       let maximo = dataJson.stations.find(station => station.slots == maxSlots)
       newText += "La estación con más slots es: "  + maximo.id + ", " + maximo.streetName + ", " + maximo.longitude + ", " + maximo.latitude + ", con " + maximo.slots + " Slots, " + maximo.bikes + "</br>";

       text += "</table>"
       document.getElementById("demo").innerHTML = text;
       document.getElementById("deemo").innerHTML = newText;
   })
   .catch(function (error) { console.log("Algo ha salido mal: "+ error) })
