let color_station;
let fillColor_station;
let latitude_station;
let longitude_station;
fetch('info.json')
   .then(function (response) {
       return response.json();
   })
   .then(function (dataJson) {
       for (const bici of dataJson.stations) {
            latitude_station = bici.latitude;
            longitude_station = bici.longitude;
            if(bici.bikes > 10){
                color_station = "green"; 
                fillColor_station = "#98FF65"        
            }
            else if(bici.bikes > 3){
                color_station = "yellow";
                fillColor_station = "#F2FF6D"
            }
            else{
                color_station = "red";
                fillColor_station = "#FF5656"
            }

            var circle = L.circle([latitude_station, longitude_station], {
                color: color_station,
                fillColor: fillColor_station,
                fillOpacity: 0.5,
                radius: 5
            }).addTo(map);
       }

   })
   .catch(function (error) { console.log("Algo ha salido mal: "+ error) })

