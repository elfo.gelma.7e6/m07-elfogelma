var tvSamsung ={
    nombre: "TV Samsung 42",
    categoria: "televisores",
    unidades: 4,
    precio: 345.95,

    getImporte(){
        return this.unidades * this.precio;
    }
}   

function GetString(){
    return JSON.stringify(tvSamsung);
}
function GetObject(json){
    return JSON.parse(json);
}