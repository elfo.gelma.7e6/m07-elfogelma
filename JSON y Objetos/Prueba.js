const requestURL = 'alumnos.json';
const request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function() {
   const jsonObj = request.response;
   console.log(jsonObj);
   let text = "<table border='1'>"
   for (const alumno of jsonObj.alumnos) {
       text += "<tr><td>" + alumno.nombre + "</td><td>" + alumno.apellido + "</td><td>" + alumno.ciclo + "</td></tr>";
   }
   text += "</table>"
   document.getElementById("demo").innerHTML = text;
}
