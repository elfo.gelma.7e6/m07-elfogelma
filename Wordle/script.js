const codigo = []; //NUMERORANDOM
const maxIntento = 8; //MAXIntentos
var count = 0; //Intentos
var jugar = true;//Indica si el juego continua o no
//Creamos constantes de las rutas para no llenar el codigo de pathings
const rutaCodigoDiv = document.getElementById("codigo").getElementsByClassName("cel flex"); 

/*1. Genera una constante CODIGO_SECRETO de tipo array de 5 número aleatorios entre 0 y 9 usando la libreria Math.random();*/
function codigoSecreto() {
    for (let i = 0; i < 5; i++) {
        codigo[i] = Math.floor((Math.random() * 10));
        //console.log(codigo[i]);
    }

}
codigoSecreto();
BeautyPage(); //Mejora la página a nivel visual

//Esto sirve para al presionar ENTER te lea lo que has excrito en input.
document.querySelector("input").addEventListener('keypress', function(event) {
    if (event.key === "Enter") {
        Comprobar();
    }
});
function Comprobar() {
    
    var numero = document.querySelector("input").value; //le damos el value a la variable numero
    var arrayNum = [];
    
    if(jugar && count <= 7){
        if(numero.length ==5 && !isNaN(numero)){           
            ComprobarNumero(numero, arrayNum);
            Comentario(); 
            count++;
            
        }else alert("Introduce un NUMERO de 5 dígitos");
    }
    
}



function ComprobarNumero(numero,  arrayNum) { 
    // Pone el numero que está correcto en la fila principal y nos mueve a la funcion CeldasGrises(),
    // 

    for(var i = 0; i<5;i++) {
        arrayNum[i] = numero[i];
        if(codigo[i] == numero[i]) rutaCodigoDiv[i].textContent = numero[i];
    }
    CeldasGrises(arrayNum);   
}


//Output del comentario
function Comentario() {
    var section = document.getElementsByClassName("w100 info");
    var info = document.getElementById("info");
    LimpiarInput();
    if(count <6){
        info.textContent = "Es tu intento nº"+(count+2);
    }
    
    if (count==maxIntento-1){
        info.textContent = "PERDISTE";
        for(var i = 0; i<5;i++) {
            rutaCodigoDiv[i].textContent = codigo[i];
        }
    } 

    if(jugar == false){
        section[0].style.backgroundColor = "green";
        info.textContent = "HAS GANADO";
    }
    if (count == maxIntento-2) info.textContent = "Es tu último intento"
}


function LimpiarInput() {
    document.querySelector("input").value = ""; //Limpiamos el valor antiguo del input
    document.querySelector("input").focus(); //funcion de js que encontramos para no tener que volver a hacer clic
}


//Funcion que sirve de puente a las funciones que modifican el style y contenido de las celdas
function CeldasGrises(arrayNum) {
    let fila = document.getElementsByClassName("rowResult w100 flex wrap");
    let celda = fila[count].getElementsByClassName("celResult flex");

    PosicionesIncorrectas(arrayNum, celda);
    
    PrintarNumeros(arrayNum, celda);
    
    //console.log(celda);
    if(count >= 1 && count < 8) CrearDiv();
    if(PosicionesCorrecta(arrayNum, celda)){
        jugar = false;
    }
}


function PosicionesCorrecta(arrayNum, celda){
    var correcto = true;
    for(var i = 0; i<celda.length;i++){       
        if(arrayNum[i] == codigo[i]) celda[i].style.backgroundColor = "green";
        else correcto = false;
        
    }
    return correcto;
}


function PosicionesIncorrectas(arrayNum, celda){
    for(var i = 0; i<celda.length;i++){
        celda[i].style.backgroundColor = "red";
        for(var j = 0; j<celda.length;j++){
            if(arrayNum[i] == codigo[j]) celda[i].style.backgroundColor = "yellow";
        }
    }
}


function PrintarNumeros(arrayNum, celda){
    for (i=0; i<celda.length;i++) {
        celda[i].textContent = arrayNum[i];
    }
}

function CrearDiv() 
{
    if(count < 7)
    {
        var section = document.getElementById("Result"); //section
        
        var div = document.createElement("div"); ///fila
        div.setAttribute("class", "rowResult w100 flex wrap");
        section.appendChild(div);
        
        for(var i = 0; i<5;i++)
        {
            //console.log("pepe");
            var subdiv = document.createElement("div"); //celda
            subdiv.setAttribute("class", "w20");
            div.appendChild(subdiv);

            var divText = document.createElement("div");
            divText.setAttribute("class", "celResult flex");
            subdiv.appendChild(divText);
        }
    }

}

function BeautyPage(){
    document.querySelector("input").value = ""; //Quita los numeros de 01234 del inicio.
    document.querySelector("input").placeholder = "Introduce un numero..."; //Pone un texto por defecto no seleccionable.
    document.querySelector("input").maxLength = 5;
    CreateButton();
    LightMode();
    
}

//Creamos el boton de modo oscuro
function CreateButton(){
    var main = document.querySelector("body");
    var nocturneButton = document.createElement("button");
    nocturneButton.setAttribute("id", "nocturne");
    nocturneButton.setAttribute("onclick", "NocturneMode()");
    main.appendChild(nocturneButton);
    nocturneButton.textContent = "Noche";
    nocturneButton.style.width = "100px";
    main.style.display = "flex";
    nocturneButton.style.flexDirection = "column";
    nocturneButton.style.height = "50px"
    
}

//Cambia al modo oscuro
function NocturneMode(){
    document.querySelector("body").style.backgroundImage = "url('firewatch.png')";
    document.querySelector("h1").style.color = "white";
    var nocturneButton = document.getElementById("nocturne").setAttribute("onclick", "LightMode()");
    document.getElementById("nocturne").textContent = "Dia";
    
}

//Cambia al modo Dia
function LightMode(){
    document.querySelector("body").style.backgroundImage = "url('f1.png')";
    document.querySelector("h1").style.color = "black";
    var nocturneButton = document.getElementById("nocturne").setAttribute("onclick", "NocturneMode()");
    document.getElementById("nocturne").textContent = "Noche";

}