function AddClassP(){
    var contador = 0;
    var p = document.querySelectorAll("p");
    for(const i of p){
        contador++;
        i.classList.add("parrafo"+contador);
    }
}

function DifferentButton(){
    var input = document.querySelectorAll("p input");
    var contador = 0;
    for(const i of input){
        if(contador == 0) i.style.backgroundColor = "red";
        else i.style.backgroundColor = "green";
        contador++;
    }
}

function ColorLabel(){
    var label = document.querySelectorAll("label input");
    for(const i of label){
        if(i.id == "input2") i.style.backgroundColor = "blue";
    }
}