function PrimerLipsum(){
    console.log(document.querySelector("div p:first-child"));
    console.log(document.getElementById("lipsum").getElementsByTagName("p")[0]);
}

function SegundoLipsum(){
    console.log(document.querySelector("div p:nth-child(2)"));
    console.log(document.getElementById("lipsum").getElementsByTagName("p")[0]);
}

function LastItem(){
    console.log(document.querySelector("li:last-of-type"));
}

function EscogeSexo(){
    console.log(document.querySelector("label:last-of-type"));
}