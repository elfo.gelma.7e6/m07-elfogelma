const semana = ["Lunes","Martes","Miércoles","Jueves","Viernes","Sábado","Domingo"];

//Primer punto
console.log(semana.find(dia => dia[0].toUpperCase() == 'M'));

//Segundo Punto
console.log(semana.findIndex(dia => dia[0].toUpperCase() == 'V'));

//Tercer Punto
console.log(semana.some(dia => dia[0].toUpperCase() == 'S'));

//Cuarto Punto
console.log(semana.every(dia => dia[dia.length-1].toLowerCase() == 's'));

