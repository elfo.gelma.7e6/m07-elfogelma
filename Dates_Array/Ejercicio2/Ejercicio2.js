const modulos = ["m02", "m03", "m04", "m05", "m06", "m07", "m08", "m08", "FOL"]
const alumnoNotas = [[8, 9, 4], [9, 10], [6, 8, 10], [4], [8, 4, 7], [], [7, 5, 9, 10],[10]]


let map = RelacionNotasModulo(Setear());
//Parte 1
function RelacionNotasModulo(array){
    let map1 = new Map();
    array.forEach((modul, idx) => {map1.set(modul ,alumnoNotas[idx])});
    return map1;
}


//Parte 2
function Actualizar(map1){
    map1.set("m07",[7.5, 6]);
    return (map1.get("m07"));
}


//Parte 3
function Setear(){
    modulosNotas = Array.from(new Set(modulos));
    modulosNotas.forEach(modulo => console.log(modulo));
    return modulosNotas;
}

