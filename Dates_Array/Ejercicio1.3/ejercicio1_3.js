const notas = [3.4, 7.9, 8.0, 2.5, 5.6, 5.4, 9.0];
console.log(Media(notas));

//Punto 1
function Media(nota){
    suma = nota.reduce((total,nota) => total + nota, 0);
    return suma / nota.length;
}

//Punto 2
function NotaSuperiorA5(notas){
    return notas.find(nota => nota > 5);
}