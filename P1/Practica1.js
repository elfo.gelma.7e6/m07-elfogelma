function Practica1(){
    var correct = false;
    do{
        //El password no es demana, ja que en les instruccions simplement es té en compte com si ja existís la contrasenya.
        var password = "Daaan";
        //En cap moment s'especifica que hagi de declarar-se a 0 totes les variables.
        var contadorMayus = 0;
        var contadorMinus = 0;
        var contadorDigit = 0;
        var letrasSeguidas = 0;

        if(password.length > 8 && password.length < 20){
            for(i = 0; i < password.length; i++){
                if(password.charCodeAt(i) > 96 && password.charCodeAt(i) < 123) contadorMinus++;
                if(password.charCodeAt(i) > 64 && password.charCodeAt(i) < 91) contadorMayus++;
                if(password.charAt(i) >= 0 && password.charAt(i) <= 9) contadorDigit++;
                if(password.charAt(i) == password.charAt(i+1) && password.charAt(i) == password.charAt(i+2)) letrasSeguidas++;
            }
            if(contadorMayus >= 1 && contadorMinus >= 3 && contadorDigit >= 2 && letrasSeguidas == 0) correct = true;
            else{
                if(contadorMayus < 1) alert("No tiene mayúsculas.");
                if(contadorMinus < 3) alert("Necesita 3 minúsculas mínimo.");
                if(contadorDigit < 2) alert("Necesita 2 dígitos mínimo.");
                //Aquesta última condició falta, però ells no l'han tingut en compte en el analisis.
                if(letrasSeguidas != 0) alert("No se pueden poner tres caràcteres iguales seguidos.");
            }
        }
    
    }while(correct == false);
    //Si la contrasenya no es correcte de primeres mai sortirà i crearà un bucle infinit ja que en cap moment es canvia.
    //De igual manera si es correcte de primeres s'acabarà.
    alert("La contraseña es correcta.");
}
